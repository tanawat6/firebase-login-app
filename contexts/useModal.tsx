import AlertMessageModal from 'components/BaseModal/AlertMessageModal';
import ConfirmModal from 'components/BaseModal/ConfirmModal';
import SuccessModal from 'components/BaseModal/SuccessModal';
import { createContext, ReactNode, useContext, useState } from 'react';

interface ShowSuccessProps {
  title?: string;
  msg?: string;
  onClose?: () => void;
}
interface ShowConfirmProps {
  title?: string;
  msg?: string;
  confirmBtnName?: string;
  cancelBtnName?: string;
  danger?: boolean;
  onOk?: () => void;
  onCancel?: () => void;
  singleButton?: boolean;
  showMsg?: boolean;
}

interface ShowErrorProps {
  title?: string;
  msg?: string;
}

const ModalContext = createContext<{
  showError: (props?: ShowErrorProps) => void;
  showSuccess: (props?: ShowSuccessProps) => void;
  showConfirm: (props?: ShowConfirmProps) => void;
}>({
  showError: () => {},
  showSuccess: () => {},
  showConfirm: () => {},
});

export function ModalProvider({
  children,
  fontType = 'admin',
}: {
  children: ReactNode;
  fontType?: 'line' | 'admin';
}) {
  const [confirmModalState, setConfirmModalState] =
    useState<ShowConfirmProps>();
  const [confirmLoading, setConfirmLoading] = useState(false);

  const [successModalState, setSuccessModalState] =
    useState<ShowSuccessProps>();

  const [errorModalState, setErrorModalState] = useState<{
    title?: string;
    msg?: string;
  }>();

  const showError = (props?: ShowErrorProps) => {
    const { title = 'เกิดข้อผิดพลาด', msg = 'กรุณาลองใหม่อีกครั้ง' } =
      props || {};
    setErrorModalState({
      title,
      msg,
    });
  };

  const showSuccess = (props?: ShowSuccessProps) => {
    const { title = 'ทำรายการสำเร็จ', msg = '', onClose } = props || {};
    setSuccessModalState({
      title,
      msg,
      onClose,
    });
  };

  const showConfirm = (props?: ShowConfirmProps) => {
    const {
      title = 'ต้องการดำเนินการต่อใช่ไหม?',
      msg = 'กดยืนยันเพื่อดำเนินการต่อ',
      confirmBtnName = 'ยืนยัน',
      cancelBtnName = 'ยกเลิก',
      danger = false,
      onOk,
      onCancel,
      singleButton = false,
      showMsg = true,
    } = props || {};

    setConfirmModalState({
      title,
      msg,
      confirmBtnName,
      cancelBtnName,
      danger,
      onOk,
      onCancel,
      singleButton,
      showMsg,
    });
  };

  const handleOnCloseSuccess = async () => {
    setSuccessModalState(undefined);
    if (successModalState?.onClose) {
      await successModalState.onClose();
    }
  };

  const closeConfirm = () => {
    if (confirmModalState?.onCancel) {
      confirmModalState.onCancel();
    }
    setConfirmModalState(undefined);
  };

  const handleOnOk = async () => {
    try {
      if (confirmModalState?.onOk) {
        setConfirmLoading(true);
        await confirmModalState.onOk();
        setConfirmLoading(false);
      }
    } catch (e) {
      throw e;
    } finally {
      setConfirmModalState(undefined);
    }
  };

  return (
    <ModalContext.Provider
      value={{
        showError,
        showSuccess,
        showConfirm,
      }}
    >
      <AlertMessageModal
        fontType={fontType}
        title={errorModalState?.title}
        visible={!!errorModalState}
        danger
        onClose={() => setErrorModalState(undefined)}
        msg={errorModalState?.msg}
      />
      <SuccessModal
        fontType={fontType}
        title={successModalState?.title}
        msg={successModalState?.msg}
        visible={!!successModalState}
        onClose={handleOnCloseSuccess}
      />
      <ConfirmModal
        fontType={fontType}
        loading={confirmLoading}
        title={confirmModalState?.title}
        okButtonName={confirmModalState?.confirmBtnName}
        msg={confirmModalState?.msg}
        visible={!!confirmModalState}
        danger={confirmModalState?.danger}
        onCancel={closeConfirm}
        cancelButtonName={confirmModalState?.cancelBtnName}
        onOk={handleOnOk}
        singleButton={confirmModalState?.singleButton}
        showMsg={confirmModalState?.showMsg}
      />
      {children}
    </ModalContext.Provider>
  );
}

export const useModal = () => useContext(ModalContext);
