import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from 'react';
import {
  User,
  getAuth,
  signInWithCustomToken,
  setPersistence,
  inMemoryPersistence,
} from 'firebase/auth';
import get from 'lodash/get';
import app from 'services/firebase';
import axios from 'axios.config';
import Cookies from 'js-cookie';
import firebase from 'firebase/app';
import AuthService from 'services/authService';
export interface AccountUser {
  uid: string;
  email: string | null;
  name: string | null;
  department: string | undefined;
  hospitalBranch: string | undefined;
  position: string | undefined;
  role: string | undefined;
}

export interface UserAttributes {
  department: string | undefined;
  hospital_branch: string | undefined;
  position: string | undefined;
  role: string | undefined;
}

const formatAuthUser = (user: User) => {
  const customAttributesJson = get(
    user,
    'reloadUserInfo.customAttributes',
    '{}',
  );
  const customAttributes = JSON.parse(customAttributesJson) as UserAttributes;
  return {
    uid: user.uid,
    email: user.email,
    name: user.displayName,
    department: customAttributes.department,
    hospitalBranch: customAttributes.hospital_branch,
    position: customAttributes.position,
    role: customAttributes.role,
  };
};

const AuthUserContext = createContext<{
  authUser: AccountUser | null;
  loading: boolean;
  signOut: () => Promise<void>;
  verifySession: (
    onSessionValid: () => void,
    onSessionInvalid: () => void,
  ) => Promise<void>;
}>({
  authUser: null,
  loading: true,
  signOut: async () => {},
  verifySession: async () => {},
});

export function AuthUserProvider({ children }: { children: ReactNode }) {
  const [authUser, setAuthUser] = useState<AccountUser | null>(null);
  const [loading, setLoading] = useState(true);
  const auth = getAuth(app);

  const initAxiosHeader = (token: string) => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  };

  const authStateChanged = async (authState: User | null) => {
    if (!authState) {
      setAuthUser(null);
      setLoading(false);
      return;
    }
    setLoading(true);
    const token = await auth.currentUser?.getIdToken();
    if (token) {
      await initAxiosHeader(token);
    }
    const formattedUser = formatAuthUser(authState);
    setAuthUser(formattedUser);
    setLoading(false);
  };

  const verifySession = async (
    onSessionValid: () => void,
    onSessionInvalid: () => void,
  ) => {
    try {
      await setPersistence(auth, inMemoryPersistence);
      const customToken = (await AuthService.verifySessionToken()).customToken;
      await signInWithCustomToken(auth, customToken)
        .then(({ user }) => {
          console.log('onSessionValid '+ user.uid);
          onSessionValid();
        })
        .catch(error => {
          console.log('onSessionInvalid signInWithCustomToken');
          onSessionInvalid();
        });
    } catch (e) {
      console.log('onSessionInvalid try/catch');
      onSessionInvalid();
    }
  };

  const signOut = async () => {
    try {
      await AuthService.logout();
      await auth.signOut();
    } catch {
      console.log('logout failed');
    }
  };

  // listen for Firebase state change
  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(authStateChanged);
    return () => unsubscribe();
  }, []);

  return (
    <AuthUserContext.Provider
      value={{
        authUser,
        loading,
        signOut,
        verifySession,
      }}
    >
      {children}
    </AuthUserContext.Provider>
  );
}
// custom hook to use the authUserContext and access authUser and loading
export const useAuth = () => useContext(AuthUserContext);
