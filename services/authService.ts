import axios from 'axios.config';

interface VerifySessionResponse {
  customToken: string;
}

export interface CustomToken {
  customToken: string;
}

interface IAuthService {
  verifySessionToken(): Promise<CustomToken>;
  logout(): Promise<void>;
}

const verifySessionToken = async (): Promise<CustomToken> => {
  const res = await axios.get<VerifySessionResponse>('/verify-session');
  return {
    customToken: res.data.customToken,
  };
};

const logout = async (): Promise<void> => {
  await axios.get<Response>('/logout');
};

const AuthService: IAuthService = {
  verifySessionToken,
  logout,
};

export default AuthService;
