// path: /axios.config.js
// set defaults axios config for Next.js
import axios from 'axios';
import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig();

// axios.defaults.baseURL = 'https://poc-auth-function-hhnftriviq-as.a.run.app/';
// axios.defaults.baseURL = 'http://localhost:3001/';
axios.defaults.baseURL = 'https://poc-auth.senestia.com/';
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.withCredentials = true;

export default axios;
