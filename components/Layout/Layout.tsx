import Header from 'components/Header/Header';
import { AuthUserProvider, useAuth } from 'contexts/useAuth';
import { ModalProvider } from 'contexts/useModal';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

interface LayoutProps {
  children?: React.ReactNode;
  withBackground?: boolean;
}
interface Profile {
  userId: string;
}

const LayoutWithAuth: React.FC<LayoutProps> = ({ children }) => {
  return (
    <AuthUserProvider>
      <ModalProvider>
        <Layout>{children}</Layout>
      </ModalProvider>
    </AuthUserProvider>
  );
};

const Layout: React.FC<LayoutProps> = ({ children }) => {
  const router = useRouter();
  const { authUser, loading, verifySession } = useAuth();

  const init = async () => {
    verifySession(
      () => {
        //session valid
        //stay on
      },
      () => {
        //session invalid redirect to login
        const currentUrl = window.location.href;
        // Redirect to the login page on a different subdomain
        window.location.href =
          'https://poc-login2.senestia.com/login?redirect=' +
          encodeURIComponent(currentUrl);
        // 'https://poc-login2.senetia.com/'+'login?redirect=' +
      },
    );
  };

  // Listen for changes on loading and authUser, redirect if needed
  useEffect(() => {
    if (!loading) {
      init();
    }
  }, [authUser, loading]);

  return (
    <div className={`bg-lightgray1 bg-cover absolute inset-0 !m-0 !p-0`}>
      <Header />
      {children}
    </div>
  );
};
export default LayoutWithAuth;
