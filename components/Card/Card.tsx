interface CardProps {
  className?: string;
  children?: React.ReactNode;
}

const Card: React.FC<CardProps> = ({ className, children }) => {
  return (
    <div className={`bg-white rounded-lg shadow m-4 ${className ?? ''}`}>
      {children}
    </div>
  );
};
export default Card;
