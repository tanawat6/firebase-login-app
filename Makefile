deploy:
	docker build -t asia-southeast1-docker.pkg.dev/synphaet-synphaetlife/firebase-login-poc/login-app:latest . --platform linux/amd64
	docker push asia-southeast1-docker.pkg.dev/synphaet-synphaetlife/firebase-login-poc/login-app:latest
	gcloud run deploy poc-firebase-login-app --image=asia-southeast1-docker.pkg.dev/synphaet-synphaetlife/firebase-login-poc/login-app:latest
