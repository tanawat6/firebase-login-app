import { ReactElement } from 'react';

import Layout from 'components/Layout/Layout';
import Card from 'components/Card/Card';
import { NextPageWithLayout } from './_app';

const Info: NextPageWithLayout = () => {
  return (
    <div className="device__height flex justify-center items-center lg:h-[calc(100vh-64px)]">
      <Card className="w-[540px] p-8">
        <div className="relative">
          <h1 className="pl-4 pt-6 text-xl font-normal">เข้าสู่ระบบแล้ว</h1>
        </div>
      </Card>
    </div>
  );
};

Info.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};

export default Info;
